/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saksarak.tictoeoop;

/**
 *
 * @author LagunaZi
 */
public class Table {

    private char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private int lastrow, lastcol;
    private Player winner;
    private boolean finish = false;
    private int turncount;

    public Table(Player x, Player o) {
        this.playerX = x;
        this.playerO = o;
        currentPlayer = x;

    }

    public void showTable() {
        System.out.println("  1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println();
        }
    }

    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastrow = row;
            this.lastcol = col;
            return true;
        }
        return false;
    }

    public Player gerCurrenPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
            turncount++;
        } else {
            currentPlayer = playerX;
            turncount++;
        }
    }
    public void checkWin(){
        checkRow();
        checkCol();
        checkCross();
        checkDraw();
    }
    void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastcol] != currentPlayer.getName()) {
                return;
            }
        }
        showTable();
        finish = true;
        winner = currentPlayer;
        setstatWinLose();
    }

    private void setstatWinLose() {
        if(currentPlayer == playerO){
            playerO.win();
            playerX.lose();
        }else{
            playerO.lose();
            playerX.win();
        }
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastrow][col] != currentPlayer.getName()) {
                return;
            }
        }
        showTable();
        finish = true;
        winner = currentPlayer;
        setstatWinLose();
    }

    void checkCross() {
        if (table[0][0] == currentPlayer.getName()
                && table[1][1] == currentPlayer.getName()
                && table[2][2] == currentPlayer.getName()) {
            showTable();
            finish = true;
            winner = currentPlayer;
            setstatWinLose();
        } else if (table[2][0] == currentPlayer.getName()
                && table[1][1] == currentPlayer.getName()
                && table[0][2] == currentPlayer.getName()) {
            showTable();
            finish = true;
            winner = currentPlayer;
            setstatWinLose();
        }

    }

     void checkDraw() {
        if (turncount == 8) {
            showTable();
            finish = true;   
            playerO.draw();
            playerX.draw();
        }
        
    }
    public boolean isFinish(){
        return finish;
    } 
    public Player getWinner(){
        return winner;
    }

}

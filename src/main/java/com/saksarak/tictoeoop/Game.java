/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saksarak.tictoeoop;

import java.util.*;

/**
 *
 * @author LagunaZi
 */
public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row, col;
    char continuecheck;
    boolean gamefinish = false;
    static Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();
    }

    public void showTurn() {
        System.out.println(table.gerCurrenPlayer().getName() + " turn");
    }

    public void intput() {
        while (true) {
            System.out.println("Please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;

            if (table.setRowCol(row, col)) {
                break;
            }
            System.out.println("Error : this area is not empty !!!");

        }
    }

    public void newGame() {

        System.out.println("Do you want to continue ?");
        System.out.println("Yes, please type Y");
        System.out.println("No, please type N");
        continuecheck = kb.next().charAt(0);
        if (continuecheck == 'Y') {
            table = new Table(playerX, playerO);
        } else {
            System.out.println("Bye Bye !");
            gamefinish = true;
        }
    }

    public void run() {
        this.showWelcome();
        while (!gamefinish) {
            this.showTable();
            this.showTurn();
            this.intput();
            table.checkWin();
            if (table.isFinish()) {
                if (table.getWinner() == null) {
                    System.out.println("Draw!!");
                } else {
                    System.out.println(table.getWinner().getName() + " Win!!");
                }
                newGame();

            }

            table.switchPlayer();
        }

    }

}
